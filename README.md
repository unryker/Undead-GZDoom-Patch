### Important
---
- Not a source port. This is pretty much a GZDoom mod.
- Probably won't ever be merged in GZDoom. That's why it's here.
- I know, not many changes yet.

### Philosophy
---
- Fixes obvious mapping mistakes in vanilla IWADs. Even things that the GZDoom devs thought was [too gameplay-changing to fix when GZDoom already made considerable amounts of changes to how Doom is played.](https://forum.zdoom.org/viewtopic.php?t=60181#p1077050)
	- Textures are fixed on a per map and per case basis.
- Dial the GZDoom-isms up to 11 in good faith.
- Keep player freedom in IWADs. EG: The IWADs serve as a training grounds for unrestricted jumping, freelook, and a demo of the smaller GZDoom differences compared to Doom.

### Notes
---
- Compatible with the original IWADs and Doom Complete.
- Fixes are done using a level postprocessor.
- Fixed errors in the following maps:
	- Doom
		- E1M2: Texture inconsistency. (Non-visible in  normal gameplay.)
		- E2M4: Texture inconsistencies.
		- E3M1: Texture inconsistencies.
		- E3M9: Texture inconsistencies.
	- Doom 2
		- MAP01: Texture inconsistency.
		- MAP02: Stuck things.
		- MAP04: Brightness inconsistencies and texture inconsistency.
		- MAP05: Texture inconsistencies.
		- MAP07: Texture inconsistencies.
		- MAP08: Texture inconsistencies.
		- MAP09: Texture inconsistencies.
		- MAP012: Texture inconsistency.

### Installation
---
- Run the .ZIP or extract the .ZIP into a folder and run it.

### Known Bugs
---
- Doom
	- E2M3: Thing 26 (Demon) is blind. Can't fix through the level post processor. Looks like it can only be done with rebuildnodes in compatibility.txt that only gzdoom.pk3 loads.
- Doom 2
	- MAP27: Sector 80 has an "incorrect" ceiling flat. This might be intentional to make the red key marker stand out more.