class UndeadLevelCompatibility : LevelPostProcessor
{
	protected void Apply(Name checksum, String mapname)
	{
		switch (checksum)
		{
			case '81A4CC5136CBFA49345654190A626C09': // DOOM.WAD e1m2
			{
				SetWallTexture(134, Line.back, Side.top, "STARTAN2"); // Fix Romero's regret. Uses the texture of Linedef 140. Not visible during normal gameplay

				break;
			}

			case '1BC04D646B32D3A3E411DAF3C1A38FF8': // DOOM.WAD e2m4
			{
				SetLineFlags(680, 0, Line.ML_DONTPEGBOTTOM); // Unpeg that lower wall that scrolls upwards when opening the door. TODO: Adjust texture Y offset if possible.

				break;
			}

			case 'C4A89A481A32BFEDDEB82E818F2BDEC5': // DOOM.WAD e3m1
			{
				// Unpeg door tracks to match the rest of the doors in the entire game
				// SetLineFlags(19, 0, Line.ML_DONTPEGBOTTOM); // These don't work. Tf?
				// SetLineFlags(22, 0, Line.ML_DONTPEGBOTTOM);
				SetLineFlags(95, 0, Line.ML_DONTPEGBOTTOM);
				SetLineFlags(104, 0, Line.ML_DONTPEGBOTTOM);
				SetLineFlags(136, 0, Line.ML_DONTPEGBOTTOM);
				SetLineFlags(144, 0, Line.ML_DONTPEGBOTTOM);

				// Matches Sector 2, 35, etc.
				SetSectorTexture(20, Sector.ceiling, "FLOOR6_1");
				SetSectorTexture(21, Sector.ceiling, "FLOOR6_1");
				SetSectorTexture(23, Sector.ceiling, "FLOOR6_1");
				SetSectorTexture(29, Sector.ceiling, "FLOOR6_1");

				SetSectorTexture(11, Sector.floor, "FLAT5_5"); // Matches Sector 36.
				SetSectorTexture(13, Sector.ceiling, "FLAT10"); // Matches Sector 22, 24.

				break;
			}

			case 'FE97DCB9E6235FB3C52AE7C143160D73': // DOOM.WAD e3m9
			{
				// Matches Sector 29 of E3M1.
				SetSectorTexture(27, Sector.floor, "FLAT20");
				SetSectorTexture(27, Sector.ceiling, "FLOOR6_1");

				// Matches Sector 10, 19, etc.
				SetSectorTexture(4, Sector.ceiling, "FLOOR6_1");
				SetSectorTexture(24, Sector.ceiling, "FLOOR6_1");
				SetSectorTexture(28, Sector.ceiling, "FLOOR6_1");
				SetSectorTexture(29, Sector.ceiling, "FLOOR6_1");
				SetSectorTexture(36, Sector.ceiling, "FLOOR6_1");
				SetSectorTexture(38, Sector.ceiling, "FLOOR6_1");

				// Matches Sector 5, 6, 20 of E3M1.
				SetSectorTexture(3, Sector.floor, "FLAT5_4");
				SetSectorTexture(4, Sector.floor, "FLAT5_4");

				SetSectorTexture(66, Sector.floor, "FLAT5_5"); // Matches Sector 11, 36 of E3M1.
				SetSectorTexture(68, Sector.ceiling, "FLAT10"); // Matches Sector 41, 37.
				SetWallTexture(146, Line.front, Side.top, "SP_HOT1"); // Matches Linedef 147.
				SetWallTexture(154, Line.front, Side.top, ""); // Deletes paperthin ceiling.
				SetWallTexture(91, Line.back, Side.bottom, "STEP4"); // Fixes HOM using texture of Linedef 91 from E3M1.

				break;
			}

			case '3C9902E376CCA1E9C3BE8763BDC21DF5': // DOOM2.WAD map01
			{
				SetSectorTexture(29, Sector.floor, "RROCK09"); // Match the rest of the surrounding floor.

				break;
			}

			case 'AB24AE6E2CB13CBDD04600A4D37F9189': // DOOM2.WAD map02
			{
				SetThingXY(85, 944, 912); // Unstuck Shotgunguy.
				SetThingXY(69, 464, 800); // Unstuck barrel.

				break;
			}

			case 'CEC791136A83EEC4B91D39718BDF9D82': // DOOM2.WAD map04
			{
				// Fix Sector lighting to match light levels when lights are switched back off.
				SetSectorLight(0, 35);
				SetSectorLight(1, 35);
				SetSectorLight(2, 35);
				SetSectorLight(3, 35);
				SetSectorLight(4, 35);
				SetSectorLight(48, 35);
				SetSectorLight(51, 35);
				SetSectorLight(52, 35);
				SetSectorLight(53, 35);
				SetSectorLight(54, 35);
				SetSectorLight(55, 35);
				SetSectorLight(56, 35);
				SetSectorLight(58, 35);
				SetSectorLight(79, 35);

				// Lower light levels underneath the doors to the dark hallway to make this look not as ugly.
				SetSectorLight(6, 80);
				SetSectorLight(77, 80);

				SetSectorTexture(10, Sector.floor, "SLIME15"); // Matches Sector 7, 5, etc.

				break;
			}

			case '9E061AD7FBCD7FAD968C976CB4AA3B9D': // DOOM2.WAD map05
			{
				SetSectorTexture(104, Sector.ceiling, "CRATOP2"); // Matches Sector 89.
				SetSectorTexture(125, Sector.ceiling, "FLAT1_2"); // Matches Sector 113, 116, etc...

				break;
			}

			case '291F24417FB3DD411339AE82EF9B3597': // DOOM2.wad map07
			{
				// This pillar matches the others now.
				SetWallTexture(21, Line.front, Side.mid, "BRICK4");
				SetWallTexture(22, Line.front, Side.mid, "BRICK4");
				SetWallTexture(23, Line.front, Side.mid, "BRICK4");
				SetWallTexture(24, Line.front, Side.mid, "BRICK4");

				break;
			}

			case '66C46385EB1A23D60839D1532522076B': // DOOM2.WAD map08
			{
				SetWallTexture(232, Line.front, Side.bottom, "STEP2"); // Matches the rest of the staircases

				// Unpeg computer textures so they don't weirdly scroll. Likely unintentional.
				SetLineFlags(270, 0, Line.ML_DONTPEGBOTTOM);
				SetLineFlags(276, 0, Line.ML_DONTPEGBOTTOM);

				SetWallTexture(598, Line.front, Side.mid, "GRAY5"); // Matches the rest of the walls.

				break;
			}

			case '82256F04136ADB2413BFC604B5F6ADF3': // DOOM2.wad map09
			{
				SetSectorTexture(91, Sector.ceiling, "FLAT1"); // Matches the ceiling above it that also has BIGBRIK2 on the sides.

				// Matches the surrounding walls and ceiling.
				SetWallTexture(292, Line.front, Side.top, "BIGBRIK1");
				SetWallTexture(292, Line.front, Side.bottom, "BIGBRIK1");

				break;
			}

			case '1AF4DEC2627360A55B3EB397BC15C39D': // DOOM2.wad map12
			{
				SetSectorTexture(119, Sector.floor, "FLAT10"); // Matches the rest of the lowering doors.

				break;
			}
		}
	}
}